customers                |
| employees                |
| offices                  |
| orderdetails             |
| orders                   |
| payments                 |
| productlines             |
| products




1) SELECT customerName
FROM customers
WHERE country = "Philippines";


2) SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";


3) SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

4) SELECT firstName, lastName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";


5) SELECT customerName
FROM customers
WHERE state IS NULL;


6) SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson"
AND firstName = "Steve";


7) SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA"
AND creditLimit > 3000;


8) SELECT * FROM customers WHERE customerName NOT LIKE "%a%";


9) SELECT * FROM orders WHERE comments LIKE "%DHL%";


10) SELECT * FROM productLines WHERE textDescription LIKE "%state of the art%";


11) SELECT DISTINCT country FROM customers;


12) SELECT DISTINCT status FROM orders;


13) SELECT customerName, country
FROM customers
WHERE country = "USA"
OR country = "France"
OR country = "Canada";


14) SELECT firstName, lastName, officeCode
FROM employees
WHERE officeCode = "5";


15) SELECT customerName
FROM customers
WHERE salesRepEmployeeNumber = 1166;


16) SELECT products.productName, customers.customerName FROM products JOIN customers ON products.productCode = customers.customerNumber WHERE customerName = "Baane Mini Imports";




17) SELECT employees.firstName, employees.lastName, customers.customerName, employees.officeCode 
FROM customers JOIN employees ON customers.customerNumber = employees.employeeNumber 
JOIN offices ON employees.employeeNumber = offices.officeCode
WHERE customers.city = offices.officeCode;




18) SELECT firstName, lastName
FROM employees
WHERE reportsTo = 1143;


19) SELECT MAX(MSRP) AS highest_MSRP, productName
FROM products;


20) SELECT COUNT(customerNumber)
FROM customers
WHERE country = "UK";


21) SELECT COUNT(productName) AS "Number of Products", productLine
  FROM products 
  GROUP BY productLine;



22) SELECT COUNT(customerName), salesRepEmployeeNumber
FROM customers
GROUP BY salesRepEmployeeNumber;


23) SELECT productName, quantityInStock
FROM products
WHERE productLine = "Planes"
AND quantityInStock < 1000;









